
[1mFrom:[0m /home/ec2-user/environment/sample_app/app/controllers/sessions_controller.rb @ line 7 SessionsController#create:

     [1;34m5[0m: [32mdef[0m [1;34mcreate[0m
     [1;34m6[0m:   binding.pry
 =>  [1;34m7[0m:   @user = [1;34;4mUser[0m.find_by([35memail[0m: params[[33m:session[0m][[33m:email[0m].downcase)
     [1;34m8[0m:   [32mif[0m @user && @user.authenticate(params[[33m:session[0m][[33m:password[0m])
     [1;34m9[0m:     log_in @user
    [1;34m10[0m:     params[[33m:session[0m][[33m:remember_me[0m] == [31m[1;31m'[0m[31m1[1;31m'[0m[31m[0m ? remember(@user) : forget(@user)
    [1;34m11[0m:     redirect_to @user
    [1;34m12[0m:   [32melse[0m
    [1;34m13[0m:     flash.now[[33m:danger[0m] = [31m[1;31m'[0m[31mInvalid email/password combination[1;31m'[0m[31m[0m
    [1;34m14[0m:     render [31m[1;31m'[0m[31mnew[1;31m'[0m[31m[0m
    [1;34m15[0m:   [32mend[0m
    [1;34m16[0m: [32mend[0m

